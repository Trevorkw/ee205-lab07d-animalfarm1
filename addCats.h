/*
#University of Hawaii, College of Engineering
#brief   Lab07d - Animal Farm 1 - EE 205 - Spr 2022
#
# 
# @file    addCats.h
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @20 Feb 2022 
*/

#pragma once

extern int valid(char catName[], float catWeight);
extern int addCat(char addName[], enum Gender addGender, enum Breed addBreeed, enum Color addCollarColor1, enum Color addCollarColor2, unsigned long long addLicense, bool addFixed, float addWeight);
