/*
#University of Hawaii, College of Engineering
#brief   Lab07d - Animal Farm 1 - EE 205 - Spr 2022
#
#
# @file    catDatabase.h
# @author  Trevor Chang <@Trevorkw@hawaii.edu>
# @date    Mar 8 2022
*/

#pragma once

#include <stdbool.h> 
#define MAX_CATS 1024
#define MAX_CAT_NAME 50


enum Gender{MALE, FEMALE, UNKNOWN_GENDER};  //male, female, unknown
enum Breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};     // unknown, main coon, manx, shorthair, persian, sphynx
enum Color{BLACK, WHITE, RED, BLUE, GREEN, PINK, ORANGE, NONE};


struct Cat {
   char name[MAX_CAT_NAME]    ;
   enum Gender catGender      ;
   enum Breed catBreed        ;
   enum Color catCollarColor1 ;
   enum Color catCollarColor2 ;
   bool isFixed               ;
   float weight               ;
   unsigned long long license ;
};

extern struct Cat cats[];
extern int catCount;


/*
extern enum Gender catGender[MAX_CATS];                                //make array for gender
extern enum Breed catBreed[MAX_CATS];                                  //make array for breed

extern char name[][MAX_CAT_NAME]; //max size 30 character array for name
extern bool isFixed[];                                                      //bool is fixed
extern float weight[];                       //weight cannot be 0, use for and if loop?

extern int catCount;
*/ 
