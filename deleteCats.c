/*
#University of Hawaii, College of Engineering
#brief   Lab07d - Animal Farm 1 - EE 205 - Spr 2022
#
#
# @file    deletCats.c
# @author  Trevor Chang <@Trevorkw@hawaii.edu>
# @date    Mar 8 2022
*/

#include <stdio.h>
#include <string.h>

#include "catDatabase.h"
#include "deleteCats.h"

void deleteAllCats() {
   printf("All cats deleted\n");
 
   for(int i = 0; i<= MAX_CATS; i++){
   	
      strcpy(cats[i].name, "x");
      cats[i].isFixed = false;
      cats[i].weight = 0;
      cats[i].catGender = UNKNOWN_GENDER;
      cats[i].catBreed = UNKNOWN_BREED;
      cats[i].catCollarColor1 = NONE;
      cats[i].catCollarColor2 = NONE;
      catCount = 0;
      return;  //seg faults without this
   }
}
