/*
#University of Hawaii, College of Engineering
#brief   Lab07d - Animal Farm 1 - EE 205 - Spr 2022
#
#
# @file    reportCats.c
# @author  Trevor Chang <@Trevorkw@hawaii.edu>
# @date    Mar 8 2022
*/


#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "reportCats.h"


int printCat(int catIndex){
   if(catIndex < 0 || catIndex > MAX_CATS) {
      printf("animalFarm0: Bad cat [%d]\n", catIndex);
      return 1;
   }
   printf("cat index = [%u] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] Collar Color 1 =[%d] Collar Color 2 =[%d] License =[%llu]\n" , catIndex, cats[catIndex].name, cats[catIndex].catGender, cats[catIndex].catBreed, cats[catIndex].isFixed, cats[catIndex].weight, cats[catIndex].catCollarColor1,cats[catIndex].catCollarColor2, cats[catIndex].license);
   return 0;
}

int printAllCats(){
   for(int i = 0; i < catCount; i++){
       printCat(i);
   }
   return 1;
}

int findCat(char catSearch[]) {
   for(int i = 0; i < catCount; i++){
      if(strcmp(catSearch, cats[i].name) == 0){
         return i;
      }
   }
   printf("There is no cat with the name %s\n", catSearch);
   return -1;
}
