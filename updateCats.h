/*
#University of Hawaii, College of Engineering
#brief   Lab07d - Animal Farm 1 - EE 205 - Spr 2022
#
#
# @file    updateCats.h
# @author  Trevor Chang <@Trevorkw@hawaii.edu>
# @date    Mar 8 2022
*/

#pragma once

extern int updateCatName(int index, char newName[]);
extern void fixCat(int index);
extern int updateCatWeight(int index, float newWeight);
