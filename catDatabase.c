/*
#University of Hawaii, College of Engineering
#brief   Lab07d - Animal Farm 1 - EE 205 - Spr 2022
#
#
# @file    catDatabase.c
# @author  Trevor Chang <@Trevorkw@hawaii.edu>
# @date     Mar 8 2022
*/

#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "addCats.h"

//make the max size of the array to be only 30 cats 
int catCount;

enum Gender catGender[MAX_CATS];
enum Breed catBreed[MAX_CATS];
enum Color catCollarColor1[MAX_CATS];
enum Color catCollarColor2[MAX_CATS];

char name[MAX_CATS][MAX_CAT_NAME];
bool isFixed[MAX_CATS];
float weight[MAX_CATS];
unsigned long long license[MAX_CATS];

struct Cat cats[1];  //warning if don't include
