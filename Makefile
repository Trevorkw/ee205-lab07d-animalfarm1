
###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial implementation
###
### Animal Farm 1
###
### @author  Trevor Chang <@trevorkw@hawaii.edu>
### @@date   Mar 8 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################



TARGET = main
all:  $(TARGET)
CC     = gcc

CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean all

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c 
	

main: main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o  #doesn't work when named differently
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o
